use std::ffi::{c_char, CStr, CString};
use ammonia::clean;

#[no_mangle]
pub extern "C" fn purify(content: *const c_char) -> *const c_char {
    let res = unsafe { CStr::from_ptr(content) }
        .to_str()
        .unwrap();

    let output = clean(res);
    CString::new(output).unwrap().into_raw()
}
