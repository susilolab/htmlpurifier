<?php

declare(strict_types=1);

$ffi = FFI::cdef('const char* purify(const char* content);', __DIR__ . '/target/release/libphtml_purifier.so');
echo $ffi->purify('<img src="javascript:evil();" onload="evil();" />'), PHP_EOL;
// <img/>
echo $ffi->purify('<a href="/test" style="color: black;"><img src="/logo.png" onerror="javascript:;"/>Rust</a>'), PHP_EOL;
// <a href="/test" rel="noopener noreferrer"><img src="/logo.png">Rust</a>
echo $ffi->purify('<b>Bold'), PHP_EOL;
// <b>Bold</b>
